<?php

use Illuminate\Database\Seeder;
use App\Provider;
class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::create([
			'name' => 'google',
			'description' => 'jpg - Must be in aspect ratio 4:3 <br/> 2 mb size <br/>
			mp4 - 1 minutes long 
			mp3 - 30 seconds long <br />< 5mb size'
		]);

		Provider::create([
			'name' => 'snapchat',
			'description' => 'jpg, gif - Must be in aspect ratio 16:9 <br/> 5 mb size <br/>
			mp4, mov - 5 minutes long <br /> 50 mb size'
		]);
    }
}
