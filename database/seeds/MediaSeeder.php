<?php

use Illuminate\Database\Seeder;
use App\Media;
use Illuminate\Support\Str;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) { 
			$type = 2;
			$fileName = Str::random(10).'.mp4';
			$priviewFile = Str::random(10).'.jpg';
			if ($i < 5) {
				$type = 1;
				$fileName = Str::random(10).'.jpg';
				$priviewFile = NULL;
			}
			$provider = 1;
			if ($i > 1 && $i < 7) {
				$provider = 2;
			}
			Media::create([
				'provider_id' => $provider,
				'name' => Str::random(10),
				'type' => $type,
				'file_name' => $fileName,
				'preview_image' => $priviewFile
			]);
		}
    }
}
