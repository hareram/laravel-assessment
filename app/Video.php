<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $table = 'media';
    protected $fillable = ['provider_id', 'name', 'type', 'file_name', 'preview_image'];
}
