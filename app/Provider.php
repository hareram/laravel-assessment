<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ['name', 'description'];

    /**
     * Get the medias for the blog post.
     */
    public function medias()
    {
        return $this->hasMany(Media::class);
    }
}
