<?php

namespace App\Http\Controllers;

use App\Media;
use App\Provider;
use App\Http\Resources\MediaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = request('type');
        $date = request('date');
        $records = Media::filter([$type, $date])
        ->orderBy('id','desc')
        // ->toSql();
        ->paginate();
        // print_r($records);
        // die;
        return MediaResource::collection($records);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (empty($data['provider_id'])) {
            return response(['error' => ['provider_id'=>'The provider id field is required.']]);
        }
        $provider = Provider::find($data['provider_id']);
        $providerName = strtolower($provider->name);

        $rules = [
            'google' => 'required|image|mimes:jpg|size:2048|dimensions:ratio=4/3',
            'snapchat' => 'required|image|mimes:jpg,gif|size:5120|dimensions:ratio=16/9'
        ];

        $message['google'] = [
            'size' => 'The :attribute must be exactly 2mb.',
            'dimensions' => 'The :attribute must be in aspect ratio 4:3.'
        ];
        $message['snapchat'] = [
            'size' => 'The :attribute must be exactly 5mb.',
            'dimensions' => 'The :attribute must be in aspect ratio 16:9.'
        ];

        $validator = Validator::make($data, [
            'name' => 'required',
            'image_file' => isset($rules[$providerName]) ? $rules[$providerName] : 'required|image|mimes:jpg,gif',
            ]
            ,[
                'image_file.size' => isset($message[$providerName]['size']) ? $message[$providerName]['size'] : 'The image file should be in proper size.',
                'image_file.dimensions' => isset($message[$providerName]['dimensions']) ? $message[$providerName]['dimensions'] : 'The image file has invalid image dimensions.',
            ]
        );

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }
        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
            $name = time() .'.'. $request->image_file->extension();
            $request->image_file->move(public_path('images'), $name);
            $data['file_name'] = $name;
            $data['type'] = 2;
        }
        $media = Media::create($data);
        return response(['media' => new MediaResource($media), 'message' => 'Created successfully'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        return response(['media' => new MediaResource($media), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        $data = $request->all();
        if (empty($data['provider_id'])) {
            return response(['error' => ['provider_id'=>'The provider id field is required.']]);
        }
        $provider = Provider::find($data['provider_id']);
        $providerName = strtolower($provider->name);

        $rules = [
            'google' => 'required|image|mimes:jpg|size:2048|dimensions:ratio=4/3',
            'snapchat' => 'required|image|mimes:jpg,gif|size:5120|dimensions:ratio=16/9'
        ];

        $message['google'] = [
            'size' => 'The :attribute must be exactly 2mb.',
            'dimensions' => 'The :attribute must be in aspect ratio 4:3.'
        ];
        $message['snapchat'] = [
            'size' => 'The :attribute must be exactly 5mb.',
            'dimensions' => 'The :attribute must be in aspect ratio 16:9.'
        ];

        $validator = Validator::make($data, [
            'name' => 'required',
            'image_file' => isset($rules[$providerName]) ? $rules[$providerName] : 'required|image|mimes:jpg,gif',
            ]
            ,[
                'image_file.size' => isset($message[$providerName]['size']) ? $message[$providerName]['size'] : 'The image file should be in proper size.',
                'image_file.dimensions' => isset($message[$providerName]['dimensions']) ? $message[$providerName]['dimensions'] : 'The image file has invalid image dimensions.',
            ]
        );

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }
        if ($request->hasFile('image_file')) {
            $file = $request->file('image_file');
            $name = time() .'.'. $request->image_file->extension();
            $request->image_file->move(public_path('images'), $name);
            $data['file_name'] = $name;
            $oldImage = $media->file_name;
            $imagePath = public_path('images/'.$oldImage);
            if (is_file($imagePath)) {
                unlink($imagePath);
            }
        }
        $media->update($data);
        return response(['media' => new MediaResource($media), 'message' => 'Updated successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy(Media $media)
    {
        $oldImage = $media->file_name;
        $imagePath = public_path('images/'.$oldImage);
        if (is_file($imagePath)) {
            unlink($imagePath);
        }
        $media->delete();
        return response(['message' => 'Deleted']);
    }
}
