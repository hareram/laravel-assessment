<?php

namespace App\Http\Controllers;

use App\Provider;
use App\Http\Resources\ProviderResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProviderResource::collection(Provider::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:255|unique:providers',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }
        $provider = Provider::create($data);
        return response(['provider' => new ProviderResource($provider), 'message' => 'Created successfully'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        return response(['provider' => new ProviderResource($provider), 'message' => 'Retrieved successfully'], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:255|unique:providers,name,'.$provider->id
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }
        $provider->update($data);
        return response(['provider' => new ProviderResource($provider), 'message' => 'Updated successfully'], 201);
    }
}
