<?php

namespace App\Http\Controllers;

use App\Video;
use App\Provider;
use App\Http\Resources\VideoResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use FFMpeg\FFMpeg;
use FFMpeg\Coordinate\TimeCode;

class VideoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if (empty($data['provider_id'])) {
            return response(['error' => ['provider_id'=>'The provider id field is required.']]);
        }
        $provider = Provider::find($data['provider_id']);
        $providerName = strtolower($provider->name);
        
        $rules = [
            'google' => 'required|mimetypes:video/mp4',
            'snapchat' => 'required|mimetypes:video/mp4,video/quicktime|size:51200'
        ];

        $validator = Validator::make($data, [
            'name' => 'required',
            'video_file' => isset($rules[$providerName]) ? $rules[$providerName] : 'required|mimetypes:video/mp4,video/quicktime',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }

        if ($request->hasFile('video_file')) {
            
            $videoObj = $request->file('video_file');
            $name = time() .'.'. $videoObj->getClientOriginalExtension();
            $videoObj->move(public_path('videos'), $name);
            $data['file_name'] = $name;
            $data['type'] = 2;

            $getID3 = new \getID3;
            $path = public_path('videos/'.$name);
            $videoFile = $getID3->analyze($path);
            $durationSeconds = $videoFile['playtime_seconds'];
            $requiredTime = round($durationSeconds / 60);
            
            if ($providerName == 'google' && $requiredTime != 1) {
                if (is_file($path)) {
                    unlink($path);
                }
                return response(['error' => ['video_file'=>'Video file should be 1 min long.']]);
            }

            if ($providerName == 'snapchat') {
                
                if ($requiredTime != 5) {
                    if (is_file($path)) {
                        unlink($path);
                    }
                    return response(['error' => ['video_file'=>'Video file should be 5 min long.']]);
                }
                $sec = 150;
                $preview = time().'.png';
                $ffmpeg = FFMpeg::create();
                $videoFF = $ffmpeg->open($path);
                $frame = $videoFF->frame(TimeCode::fromSeconds($sec));
                $frame->save(public_path('videos/'.$preview));
                $data['preview_image'] = $preview;
            }
        }
        $video = Video::create($data);
        return response(['video' => new VideoResource($video), 'message' => 'Created successfully'], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $data = $request->all();
        if (empty($data['provider_id'])) {
            return response(['error' => ['provider_id'=>'The provider id field is required.']]);
        }
        $provider = Provider::find($data['provider_id']);
        $providerName = strtolower($provider->name);

        $rules = [
            'google' => 'required|mimetypes:video/mp4',
            'snapchat' => 'required|mimetypes:video/mp4,video/quicktime|size:51200'
        ];

        $validator = Validator::make($data, [
            'name' => 'required',
            'video_file' => isset($rules[$providerName]) ? $rules[$providerName] : 'required|mimetypes:video/mp4,video/quicktime',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        }
        
        if ($request->hasFile('video_file')) {
            
            $video = $request->file('video_file');
            $name = time() .'.'. $request->video_file->extension();
            $request->video_file->move(public_path('videos'), $name);
            $data['file_name'] = $name;
            $data['type'] = 2;

            $getID3 = new \getID3;
            $path = public_path('videos/'.$name);
            $videoFile = $getID3->analyze($path);
            $durationSeconds = $videoFile['playtime_seconds'];
            $requiredTime = round($durationSeconds / 60);
            
            if ($providerName == 'google' && $requiredTime != 1) {
                if (is_file($path)) {
                    unlink($path);
                }
                return response(['error' => ['video_file'=>'Video file should be 1 min long.']]);
            }

            if ($providerName == 'snapchat') {
                
                if ($requiredTime != 5) {
                    if (is_file($path)) {
                        unlink($path);
                    }
                    return response(['error' => ['video_file'=>'Video file should be 5 min long.']]);
                }
                $sec = 150;
                $preview = time().'.png';
                $ffmpeg = FFMpeg::create();
                $videoFF = $ffmpeg->open($path);
                $frame = $videoFF->frame(TimeCode::fromSeconds($sec));
                $frame->save(public_path('videos/'.$preview));
                $data['preview_image'] = $preview;

                $oldPreview = $video->preview_image;
                $previewPath = public_path('videos/'.$oldPreview);
                if (is_file($previewPath)) {
                    unlink($previewPath);
                }
            }

            $oldVideo = $video->file_name;
            $videoPath = public_path('videos/'.$oldVideo);
            if (is_file($videoPath)) {
                unlink($videoPath);
            }
        }

        $video->update($data);
        return response(['video' => new VideoResource($video), 'message' => 'Updated successfully'], 200);
    }
}
