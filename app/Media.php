<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = ['provider_id', 'name', 'type', 'file_name', 'preview_image'];

    /**
     * Get the provider that owns the comment.
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    public function scopeFilter($query, $filters)
	{
		if ($filters[0]) {
		  $query->where('type', '=', $filters[0]);
		}
		if ($filters[1]) {
		  $query->where('created_at', 'like', $filters[1].'%');
		}
	}
}
